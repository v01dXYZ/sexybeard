# SexyBeard 

## Purposes

Getting hurt by using C instead of Python/Numpy.
Right now, no real aim, so just trying to get used to
CBLAS / LAPACK routines.

## Overview

* Overall structure of the program programmed with Scheme/Chibi (library-less
  would be nice)
* Well thought interface, coded in C

## Status

* Right now, migrating the whole REPL implementation from C to a full Scheme one.
  Creating foreign types with nice interfaces which could be used the functional
