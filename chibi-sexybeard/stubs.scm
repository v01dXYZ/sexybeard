(c-include "global_state.h")

;(define-c-struct global_state
;  predicate: global-state?)

(c-include "substrings.h")

(define-c-struct substring_array
  predicate: substrings-array?
  finalizer: substring_array_delete)

(define-c (struct substring_array) (tokenize-string "tokenize_string") (string))
(define-c void (substring-array-print-substrings  "substring_array_print_substrings") (substring_array))
