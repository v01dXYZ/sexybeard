(import (scheme small))

(define-record-type global-state
    (make-global-state quit iteration)
    global-state?
    (quit global-state-quit)
    (iteration global-state-iteration))

(letrec* ((iterate (lambda (gs)
		     (if (not (global-state-quit gs))
			 (begin
			   (display "[")
			   (display (global-state-iteration gs))
			   (display "] >")
			   (flush-output-port)
			   (let* ((str (read-line))
				  (x (tokenize-string str)))
			     (newline)
			     (substring-array-print-substrings x)
			     (make-global-state #f (+ 1 (global-state-iteration gs)))
			     ))
			 (make-global-state #t (+ 1 (global-state-iteration gs)))
			 )))
	  (init (make-global-state #f 0))
	  (loop (lambda (i)
		  (loop (iterate i)))))
  (loop init))
