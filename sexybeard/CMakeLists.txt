add_library (sexybeard
  OBJECT
  src/global_state.c
  src/hash.c
  src/hash_map.c
  src/panic.c
  src/repl.c
  src/sexybeard.c
  src/substrings.c
  ../csv/csv.c)

file (COPY data/bike_day.csv DESTINATION .)
target_link_libraries(sexybeard openblas)
target_include_directories(sexybeard PUBLIC include)
add_dependencies(sexybeard libsleef)
target_link_libraries(sexybeard sleef)
