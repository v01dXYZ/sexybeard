#include "substrings.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hash.h"
#include "hash_map.h"
#include "panic.h"

struct substring_array substring_array_new(char *str, int size) {
  struct substring_array sba;
  sba.array = malloc(size * sizeof(struct substring));

  if (sba.array == NULL) {
    panic("Substring Array cannot allocate resources");
  }

  sba.cursor = -1;
  sba.capacity = size;
  sba.str = str;

  return sba;
}

void substring_array_delete(struct substring_array* s) {
  free(s->array);
}

struct substring substring_array_get(struct substring_array *s, int i) {
  if (i > s->cursor)
    panic("Substring Array Out of bound get");

  return s->array[i];
}

int substring_array_cmp(struct substring_array *s, int i, char *str) {
  struct substring sb = substring_array_get(s, i);

  return strncmp(s->str + sb.id, str, sb.length);
}

int substring_array_hash(struct substring_array *s, int i) {
  struct substring sb = substring_array_get(s, i);

  return sb.hash;
}
void substring_array_print_substrings(struct substring_array *s) {
  for (int i = 0; i <= s->cursor; i += 1) {
    struct substring sb = s->array[i];
    printf("	[%d] %.*s\n", i, sb.length, s->str + sb.id);
  }
}

bool substring_array_is_not_empty(struct substring_array *s) {
  return s->cursor >= 0;
}

void substring_array_reallocate(struct substring_array *s, int new_capacity) {
  if (new_capacity < s->capacity) {
    return;
  }
  struct substring *new_array = malloc(new_capacity * sizeof(struct substring));

  if (new_array == NULL) {
    panic("Substring Array Can't reallocate");
  }

  if (s->cursor >= 0) {
    memcpy(new_array, s->array, s->cursor * sizeof(struct substring));
  }

  free(s->array);
  s->array = new_array;
}

void substring_array_double_capacity(struct substring_array *s) {
  substring_array_reallocate(s, 2 * s->capacity);
}

void substring_array_append(struct substring_array *s, struct substring sb) {
  if (s->cursor == s->capacity - 1) {
    substring_array_double_capacity(s);
  }

  s->array[++s->cursor] = sb;
}

void substring_array_merge(struct substring_array *s, struct substring *sb,
                           int size) {

  substring_array_reallocate(s, s->capacity + size);
  memcpy(s->array + s->cursor + 1, sb, size * sizeof(struct substring));
  s->cursor += size;
}

char *substring_array_get_substring(struct substring_array *s,
                                    struct substring *sb) {
  return s->str + sb->id;
}

int substring_array_length(struct substring_array *s) { return s->cursor + 1; }

static bool tokenize_string_h_is_space(char c) {
  return (c == ' ') || (c == '\0') || (c == '\n') || (c == '.') || (c == ';') ||
         (c == ',');
}

struct substring_array tokenize_string(char *str) {
  struct substring_array ret = substring_array_new(str, 256);

  bool previous_is_space = 1;
  int last_rising;

  for (int i = 0;; i += 1) {
    char c = str[i];
    bool is_space = tokenize_string_h_is_space(c);

    if (previous_is_space && !is_space) {
      last_rising = i;
    }

    if (!previous_is_space && is_space) {
      struct substring sb;
      sb.length = i - last_rising;
      sb.id = last_rising;
      sb.hash = hash_string_size(str + last_rising, sb.length);
      substring_array_append(&ret, sb);
    }

    previous_is_space = is_space;

    if (str[i] == 0) {
      break;
    }
  }

  return ret;
}
