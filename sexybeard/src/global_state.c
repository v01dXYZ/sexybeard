#include "global_state.h"

#include "repl.h"

struct global_state global_state_init() {
  struct global_state ret;
  ret.quit = 0;
  ret.loop_iteration = 0;

  repl_global_state_init(&ret);

  return ret;
}

void global_state_destroy(struct global_state *s) {
  hash_table_delete(&s->handle_table);
}

bool global_state_is_quit(struct global_state *s) { return s->quit; }
