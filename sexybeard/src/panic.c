#include "panic.h"

#include <stdio.h>
#include <stdlib.h>

/**
 * panic subsystem
 * not really evolved
 */
void panic(char *str) {
  fprintf(stdout, "[Panic !] %s\n", str);
  exit(-1);
}
