#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include "hash_map.h"
#include "repl.h"
#include "substrings.h"

void print_home_page() {
  char home_page_text[] = " ------- HOME PAGE ----------- \n"
                          "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
 
  printf("%s", home_page_text);
}

int main() {
  struct global_state gs = global_state_init();

  print_home_page();

  for (int i = 0; !gs.quit; i += 1) {
    char buffer[1024];
    printf("[%d]> ", i);
    fflush(stdout);
    char *success_input = fgets(buffer, 1024, stdin);
    if (success_input) {
      repl(&gs, buffer);
    } else {
      break;
    }
  }

  global_state_destroy(&gs);

  return 0;
}
