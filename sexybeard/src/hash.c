unsigned int hash_update(char c, int hash) { return (hash << 5) + hash + c; }

unsigned int hash_string_size(char *str, int size) {
  unsigned int hash = 0;
  for (int i = 0; i < size; i += 1) {
    hash = hash_update(str[i], hash);
  }
  return hash;
}

unsigned int hash_string_null(char *str) {
  unsigned int hash = 0;
  for (int i = 0; str[i]; i += 1) {
    hash = hash_update(str[i], hash);
  }
  return hash;
}
