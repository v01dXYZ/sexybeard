#include "hash_map.h"

#include <stdio.h>
#include <stdlib.h>

#include "hash.h"
#include "panic.h"

struct hash_table hash_table_new(int size) {
  int *array_keys = malloc(size * sizeof(int));
  void **array_values = calloc(sizeof(void *), size);

  if (array_keys == 0 || array_values == 0) {
    panic("Hash Table Can't allocate ressource");
  }

  struct hash_table ht;
  ht.array_keys = array_keys;
  ht.array_values = array_values;
  ht.size = size;

  return ht;
}

void hash_table_delete(struct hash_table *s) {
  free(s->array_keys);
  free(s->array_values);
}

void hash_table_print_map(struct hash_table *s) {
  printf("%d", s->size);
  for (int i = 0; i < s->size; i += 1) {
    if (s->array_values[i]) {
      printf("Hash Map: (%p) [%d] -> [%p] \n", s, s->array_keys[i],
             s->array_values[i]);
    }
  }
}

void *hash_table_get(struct hash_table *s, unsigned int hash) {
  int id = hash % s->size;

  if (s->array_keys[id] != hash) {
    return NULL;
  }

  return s->array_values[id];
}

void hash_table_set(struct hash_table *s, unsigned int hash, void *val) {
  unsigned int id = hash % s->size;

  if (s->array_values[id]) {
    panic("Hash table collision\n");
  }

  s->array_values[id] = val;
  s->array_keys[id] = hash;
}

void *hash_table_get_string_size(struct hash_table *s, char *str, int size) {
  unsigned int hash = hash_string_size(str, size);
  return hash_table_get(s, hash);
}

void *hash_table_get_string_null(struct hash_table *s, char *str) {
  unsigned int hash = hash_string_null(str);
  return hash_table_get(s, hash);
}

void hash_table_set_string_size(struct hash_table *s, char *str, int size,
                                void *val) {
  unsigned int hash = hash_string_size(str, size);
  return hash_table_set(s, hash, val);
}

void hash_table_set_string_null(struct hash_table *s, char *str, void *val) {
  unsigned int hash = hash_string_null(str);
  return hash_table_set(s, hash, val);
}
