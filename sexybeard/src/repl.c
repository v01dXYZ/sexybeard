#include "repl.h"

#include <cblas.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "global_state.h"
#include "hash.h"
#include "hash_map.h"
#include "substrings.h"
#include "csv.h"

#include "immintrin.h"
#include "sleef.h"

static void repl_handle_quit(struct global_state *gs,
                             struct substring_array *tokens) {
  gs->quit = true;
  substring_array_delete(tokens);

  char exit_txt[] = " ---------------------------- \n "
                    "Thank you and see you soon ! \n"
                    " ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ \n";

  printf("%s", exit_txt);
}

static void repl_handle_csv(struct global_state *gs,
                            struct substring_array *tokens) {
  char * filename;

  if(substring_array_length(tokens) == 1) {
    filename = "bike_day.csv";

    struct stat file_sample;
    if(stat(filename, &file_sample) && errno == ENOENT){
      printf("sample file \"%s\" missing halting\n", filename);
      return;
    }
    
    struct csv file;
    char** fields;
    char* error;
    int ret;

    csv_open(&file, filename, ',', 14);
    csv_read_record(&file, &fields);

    const int nbr_lines = 700;
    const int nbr_feats = 4;
    float* arr = malloc(nbr_lines*nbr_feats*sizeof(float));

    struct stat status_matrix_bin;
    if(stat("matrix.bin", &status_matrix_bin) && errno == ENOENT){
      printf("matrix.bin will be created\n");
      for(int l=0; l<nbr_lines; l+=1){
        ret = csv_read_record(&file, &fields);
        if(ret != CSV_OK) {
          break;
        }

        __m128 f;
        for(int i=0; i<nbr_feats; i+=1)
          f[i] = strtof(fields[10+i], NULL);

        __m128 r = Sleef_sinf4_u10(f);

        memcpy(arr+nbr_feats*l, &r, nbr_feats*sizeof(float));
      }

      FILE * file_matrix_bin = fopen("matrix.bin", "w");
      fwrite(arr, sizeof(float), nbr_lines*nbr_feats, file_matrix_bin);
      fclose(file_matrix_bin);
    } else {
      printf("matrix.bin will be fetch from the cache\n");
      FILE * file_matrix_bin = fopen("matrix.bin", "r");
      size_t read_data_size = fread(arr, sizeof(float), nbr_lines*nbr_feats, file_matrix_bin);
      if(read_data_size != nbr_lines*nbr_feats) {
        printf("%ld / %d expected Too few data blocks ! Remove the cache file can maybe solve the problem.", read_data_size, nbr_lines*nbr_feats);
        return;
      }
    }
    
    float* C = malloc(nbr_feats*nbr_feats*sizeof(float));
    // to chew the thing
    const int m = nbr_feats;
    const int k = nbr_lines;
    const int n = nbr_feats;
    const float alpha = 1.0;

    printf("Printing A\n");
    for(int i=0; i<nbr_lines; i+=1) {
      for(int j=0; j<nbr_feats; j+=1) {
        printf("    %f ", arr[4*i+j]);
      }
      printf("\n");
    }


    // alpha*op(A)*op(B) + beta*C
    cblas_sgemm(CblasRowMajor,  // Layout
                CblasTrans,     // Transpose A
                CblasNoTrans,   // Transpose B
                m,              // number row output
                n,              // number cl output
                k,              // computation dimension
                alpha,          // alpha
                arr,            // A
                m,              // major dimension of a
                arr,            // B
                n,              // major dimension of B (not op(B))
                0,              // Beta
                C,              // output
                n);


    
    printf("Printing C\n");
    for(int i=0; i<nbr_feats; i+=1) {
      for(int j=0; j<nbr_feats; j+=1) {
        printf("    %f ", C[4*i+j]);
      }
      printf("\n");
    }

    free(C);
    free(arr);

  } else {
    struct substring sb = substring_array_get(tokens, 1);
    int length = sb.length;
    filename = malloc(length+4+1); // text + ".csv" + "\0"
    strncpy(filename, substring_array_get_substring(tokens, &sb), length);
    strncpy(filename+length, ".csv", 5);
    free(filename);

    printf("Custom csv file not implemented yet\n");
  }
}

void repl(struct global_state *gs, char *str) {
  struct substring_array tokens = tokenize_string(str);

  // DEBUG
  //  substring_array_print_substrings(&tokens);

  if (substring_array_is_not_empty(&tokens)) {

    struct substring first_word = substring_array_get(&tokens, 0);

    /*
      repl_handle handle =  hash_table_get_string_size(&gs->handle_table,
      ff
      substring_array_get_substring(&tokens, &first_word), f
      first_word.length);
    */

    repl_handle handle = hash_table_get_string_size(
        &gs->handle_table, tokens.str + first_word.id, first_word.length);
    if (handle) {
      handle(gs, &tokens);
    } else {
      printf("Error: command not found <%.*s>\n", first_word.length,
             tokens.str + first_word.id);
    }
  } else {
    printf("Error: No input\n");
  }
}

void repl_global_state_init(struct global_state *s) {
  struct hash_table ht = hash_table_new(256);

  hash_table_set_string_null(&ht, "quit", repl_handle_quit);
  hash_table_set_string_null(&ht, "q", repl_handle_quit);
  hash_table_set_string_null(&ht, "exit", repl_handle_quit);
  hash_table_set_string_null(&ht, "csv", repl_handle_csv);

  s->handle_table = ht;
}

