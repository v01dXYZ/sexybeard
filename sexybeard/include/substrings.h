#ifndef SEXYBEARD_H_SUBSTRINGS
#define SEXYBEARD_H_SUBSTRINGS

#include <stdbool.h>

/**
 * substring structure
 * - id:        start of the substrings
 * - length:    length of the substrings
 * - hash:      hash of the substring
 */
struct substring {
  int id;
  int length;
  int hash;
};

/**
 * substring array structure
 * - cursor:
 * - capacity:
 * - str:
 */
struct substring_array {
  struct substring *array;
  int cursor;
  int capacity;
  char *str;
};

/**
 * substring constructor
 */
struct substring_array substring_array_new(char *str, int size);
void substring_array_delete(struct substring_array* s);
struct substring substring_array_get(struct substring_array *s, int i);
int substring_array_cmp(struct substring_array *s, int i, char *str);
int substring_array_hash(struct substring_array *s, int i);
/**
 * Print the substrings of the array
 */
void substring_array_print_substrings(struct substring_array *s);
bool substring_array_is_not_empty(struct substring_array *s);
void substring_array_reallocate(struct substring_array *s, int new_capacity);
void substring_array_double_capacity(struct substring_array *s);
void substring_array_append(struct substring_array *s, struct substring sb);
void substring_array_merge(struct substring_array *s, struct substring *sb,
                           int size);
struct substring_array tokenize_string(char *str);
char *substring_array_get_substring(struct substring_array *s, struct substring *sb);
int substring_array_length(struct substring_array *s);
#endif
