#ifndef SEXYBEARD_H_HASH_MAP
#define SEXYBEARD_H_HASH_MAP

#include <stdbool.h>

struct array_map {
  int *array_keys;
  void **array_values;
  int size;
};

struct array_map array_map_new(int size);
void *array_map_get(struct array_map *s, int hash);
bool array_map_set(struct array_map *s, int hash, void *val);
void *array_map_get_string_size(struct array_map *s, char *str, int size);
void *array_map_get_string_null(struct array_map *s, char *str);
bool array_map_set_string_size(struct array_map *s, char *str, int size,
                               void *val);
bool array_map_set_string_null(struct array_map *s, char *str, void *val);
#endif
