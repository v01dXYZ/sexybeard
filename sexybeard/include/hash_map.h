#ifndef SEXYBEARD_H_HASH_MAP
#define SEXYBEARD_H_HASH_MAP

#include <stdbool.h>

/**
 * Hash table structure for static objects
 * Do not use it for variables living in the stack
 * or the heap because there are not destoyed aftewards
 */

struct hash_table {
  int *array_keys;
  void **array_values;
  int size;
};

struct hash_table hash_table_new(int size);
void hash_table_delete(struct hash_table *s);
void *hash_table_get(struct hash_table *s, unsigned int hash);
void hash_table_set(struct hash_table *s, unsigned int hash, void *val);
void *hash_table_get_string_size(struct hash_table *s, char *str, int size);
void *hash_table_get_string_null(struct hash_table *s, char *str);
void hash_table_set_string_size(struct hash_table *s, char *str, int size,
                                void *val);
void hash_table_set_string_null(struct hash_table *s, char *str, void *val);
void hash_table_print_map(struct hash_table *s);
#endif
