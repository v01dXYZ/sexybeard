#ifndef SEXYBEARD_H_REPL
#define SEXYBEARD_H_REPL
#include "global_state.h"
#include "substrings.h"

typedef void (*repl_handle)(struct global_state *gs,
                            struct substring_array *tokens);

void repl(struct global_state *gs, char *str);
void repl_global_state_init(struct global_state *s);
#endif
