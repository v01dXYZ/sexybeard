#ifndef SEXYBEARD_H_GLOBAL_STATE
#define SEXYBEARD_H_GLOBAL_STATE

#include <stdbool.h>

#include "hash_map.h"

struct global_state {
  bool quit;
  int loop_iteration;
  struct hash_table handle_table;
};

struct global_state global_state_init();
void global_state_destroy(struct global_state *s);
bool global_state_is_quit(struct global_state *s);
#endif
