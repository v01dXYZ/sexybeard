#ifndef SEXYBEARD_H_HASH
#define SEXYBEARD_H_HASH

unsigned int hash_update(char c, int hash);
unsigned int hash_string_size(char *str, int size);
unsigned int hash_string_null(char *str);
#endif
